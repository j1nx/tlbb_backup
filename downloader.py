import xbmcgui
import urllib
import time
import requests
import sys
import utils

def download(url, dest, dp = None):
    if not dp:
        dp = xbmcgui.DialogProgress()
        dp.create("Keyword Installer","Downloading & Copying File",' ', ' ')
    dp.update(0)
    start_time=time.time()
    urllib.urlretrieve(url, dest, lambda nb, bs, fs: utils.progress(nb, bs, fs, dp, start_time))
'''
def download(url, dest, dp = None):
    if not dp:
        dp = xbmcgui.DialogProgress()
        dp.create("Keyword Installer","Downloading & Copying File",' ', ' ')
    dp.update(0)
    with open(dest, 'wb') as f:
        start = time.clock()
        r = requests.get(url, stream=True, verify=False)
        total_length = int(r.headers.get('content-length'))
        dl = 0
        if total_length is None: # no content length header
            f.write(r.content)
        else:
            try:
                for chunk in r.iter_content(1024):
                    dl += len(chunk)
                    f.write(chunk)
                    done = int(100 * dl / total_length)
                    m_b_s = '%.02f MB of %.02f MB' % (float(dl)/(1024*1024), float(total_length)/(1024*1024))
                    #sys.stdout.write("\r[%s%s] %s bps" % ('=' * done, ' ' * (50-done), dl//(time.clock() - start)))
                    dp.update(done,m_b_s)
            except Exception as e:
                print "----------",e
'''
